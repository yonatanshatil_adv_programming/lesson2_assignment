first transform:
begin transaction;
update accounts set balance=(select balance from accounts where id=3)-300 where id=3;
update accounts set balance=(select balance from accounts where id=1)+300 where id=1;
commit;

second transform:
begin transaction;
update accounts set balance=(select balance from accounts where id=2)-200 where id=2;
update accounts set balance=(select balance from accounts where id=1)+200 where id=1;
commit;